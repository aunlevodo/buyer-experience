---
  title: What are microservices? | GitLab
  description: Microservices are a modern software architecture design where an application is split into many small parts that can work independently, allowing teams to innovate faster and achieve massive scale.
  components:
    - name: topics-header
      data:
        title: What are microservices?
        read_time: 5 min read
        block:
            - text: |
                A microservices architecture splits an application into many small services allowing teams to innovate faster and achieve massive scale.
    - name: 'topics-copy-block'
      data:
        header: Microservices explained
        column_size: 10
        blocks:
          - text: |
                A microservices application architecture involves splitting a monolithic application into many small services, allowing teams to achieve immense scale and innovate faster. Many businesses built their original services using a monolithic application architecture. In other words, all the architecture components reside within a single infrastructure.

                When developers use a microservices architecture, they break out each system component and deploy them as individual services. The services communicate with each other using API calls.

                The result is a pluggable architectural style. It allows for fast and cost-efficient upgrades. If you need to scale only one area of the system, it’s easier and less expensive than scaling the entire system. Developers use container orchestration tools and Kubernetes to appropriately allocate processing power and whatever other resources they require. These services run in a container on a cloud-based server.
    - name: 'topics-copy-block'
      data:
        header: Moving from monolith to microservices
        column_size: 10
        blocks:
          - text: |
                You can understand [microservices architecture](https://about.gitlab.com/direction/microservices/) by comparing it to a legacy monolith application architecture. With monolithic architecture, all the components are part of a single unit. It develops, deploys and scales everything together. Teams must write the app in a single language with a single runtime.

                Different teams working on different app components must coordinate, so they don't affect each other's work. For example, one part of the app may depend on a module that needs a specific version, say 1.8, while another team needs the same module but requires the 1.7 version because 1.7 is incompatible with another dependency. In a monolithic app, you have to pick one or the other. Similarly, monolithic apps deploy everything as a single application, requiring testing and deploying code together.

                With microservices, each component is broken out and deployed individually as services and the services communicate with each other via API calls.
            image:
              image_url: /nuxt-images/topics/monolith-vs-microservices-large.png
              alt: A monolith vs microservice architecture
    - name: 'topics-copy-block'
      data:
        header: Microservice core components
        column_size: 10
        blocks:
          - text: |
                Microservices maximize application reliability and deployment velocity. This is particularly important in a marketplace that's moving and evolving faster than ever. Microservices' containerized architecture moves apps anywhere without altering or disrupting the environment, facilitating speed and reducing downtime.

                Let's look at microservices' core components:

                1. **Clients**: Client apps usually must consume functionality from multiple microservices and require frequent updates.
                1. **Databases**: A service API updates microservices databases by carrying all remote services supporting inter-process communications for different stacks.
                1. **API gateway**: An API gateway is a microservices design pattern that's a vital single app entry point for routing requests and protocol translation.
                1. **Identity providers**: An identity microservice must permit server-to-server and user-driven access to identity data.
                1. **Messaging formats**: Microservices communicate with synchronous or asynchronous microservice architecture patterns.
                1. **Static content**: Clients receive static content via cloud-based storage services and content delivery networks.
                1. **Management**: This component allows business users to configure services during run-time.
                1. **Service discovery**: A service discovery mechanism is required to request service.
    - name: 'topics-copy-block'
      data:
        header: Microservice characteristics
        column_size: 10
        blocks:
          - text: |
              [Martin Fowler's quintessential article on microservices](https://martinfowler.com/articles/microservices.html) defines nine components that most microservice architectures have in common.

              ### 1. Componentization via Services
              Microservice's nature dictates breaking down each component by service. As such, it can designate, deploy, refine and redeploy each service independently without affecting the environment. This means it's usually possible to change a service instead of redeploying an entire app.

              ### 2. Organized around Business Capabilities
              With the monolithic approach, tech teams focused separately on tech-related capabilities such as UI, databases and server-side logic. On the other hand, microservices organize cross-functional teams around business capabilities and priorities. Each team designs specific products based on individual services that communicate using a message bus.

              ### 3. Products not Projects
              In the past, app developers used the project model that tasked teams to build software, which then went to a maintenance organization upon completion. Microservices architecture favors a team owning the product over its lifetime.

              This allows developers to interact with their products in new ways, seeing how they behave in production and increasing user contact. This benefits engineers and businesses to increase collaboration and understand each other's fields.

              ### 4. Smart endpoints and dumb pipes
              Microservices have a lot in common with the traditional UNIX system. They receive a request, process it and generate an appropriate response. Fowler refers to this approach as "smart endpoints and dumb pipes." The infrastructure is usually dumb as it serves solely as a message router, and all the smarts reside at the endpoints producing and consuming endpoints.

              ### 5. Decentralized Governance
              Decentralized governance is microservice architecture's default structure because single-technology platforms often lead to over-standardization. A major advantage of microservices vs. monoliths is using different languages where they're best suited. For example, Spring Boot microservices can build an app for one component, with Spring Cloud comprising another.

              ### 6. Decentralized Data Management
              Most microservices allow each component to manage its own database from decentralized data management. You can always use the best data store for a specific project while removing the time-consuming task of shared database upgrades.

              ### 7. Infrastructure Automation
              CI/CD experts use infrastructure automation in microservices. It lessens developers' workloads and significantly improves deployment timeline efficiency.

              ### 8. Design for failure
              Like the best businesses, microservices have resilience in mind. As unique and diverse services must communicate, failure is likely. This is microservices' main disadvantage compared to monoliths because the solution requires additional complexity.

              Sophisticated monitoring and logging setups are necessary to prevent failure from impacting consumers. While this requires more work for engineers, it means fail resistance is well-developed.

              ### 9. Evolutionary Design
              With such a fast-paced tech industry, evolutionary design is no longer a luxury; it's a necessity. More new electronic device types hit the market each year, and your apps must be ready to facilitate them. Microservices' deconstructed design means you can give apps a makeover without redeployment.

              Fowler went into more detail about each of these components in this talk from GOTO.
            video:
              video_url: https://www.youtube-nocookie.com/embed/wgdBVIX9ifA
    - name: topics-copy-block
      data:
        header: Microservice architecture use cases
        column_size: 10
        blocks:
          - text: |
              Microservices designs are agile and flexible and enable minimal downtime for maintenance, repairs and updates.

              Some key use cases for microservices include:
              ### 1. Data processing
              Apps running on microservice architecture can handle more concurrent requests in less time, making them more efficient.
              ### 2. Website migration
              Microservices avoid site UI, structure and/or domain site migration downtime.
              ### 3. Large-scale media
              Platforms can handle an extreme number of requests for different subdomains without errors or delays.
              ### 4. Invoices and transactions
              Microservices can help make transactions more robust with reduced app failure, allowing companies to scale up without redeploying.
              ### 5. Outdated systems
              Developing a new containerized system is more efficient and effective than updating old, clunky monoliths.

    - name: topics-copy-block
      data:
        header: Engineering benefits of microservices
        column_size: 10
        blocks:
          - text: |
              [Implementing a microservices architecture](/blog/2019/06/17/strategies-microservices-architecture/){data-ga-name="Microservice architecture"}{data-ga-location="body"}, or decomposing a legacy monolith into microservices, can increase velocity, flexibility and scalability, often at simplicity's cost. Monoliths are straightforward to build, deploy and debug, but hard to scale. While a microservice architecture is more complex, there are several benefits for engineers:

              ### 1. Independent deployment
              Each microservice must be a full-stack to function correctly. Development teams can easily fix errors on a single microservice without impacting the whole environment.

              ### 2. Scalability
              Teams building apps with microservices architecture can change each service without disrupting the rest of the app. Programmers can work in parallel to identify hot services, update functionality and scale selected services.

              ### 3. Tech stack freedom
              Reliance on the singular tech stack is one of monolithic architecture's most limiting features. With microservices, developers can build an app using the programming language, framework, databases and front-end and backend tools. This mitigates compromising on a jack-of-all-trades and master of none standardized stack.

              ### 4. Fault isolation
              A single fault often means an entire monolith app fails. Microservices isolate faults to their components, so the whole application isn't affected.

              ### 5. Productivity
              Microservices reduce silos and promote cross-functional teamwork for today's distributed and remote workforces. They can be tested independently while developers handle other aspects, streamlining quality assurance.
    - name: topics-copy-block
      data:
        header: Business value of microservices
        column_size: 10
        blocks:
          - text: |
              For medium and large businesses, the time is now. There has never been a better moment to move your architecture from a monolithic structure to one relying on microservices. It will save you money in the long run, make your business operate more efficiently and keep your customers happier. Microservices are the architecture of the future, and it's time that you find out how they can help your business.

              With business value, microservices have a lot of benefits compared to monoliths. They promote a symbiotic relationship between developers and business leaders, leading to better outcomes. Microservices promote:

              ### 1. Faster pace of innovation
              Their evolutionary design makes it easy to change and upgrade application components.
              ### 2. Greater stability/resiliency
              Microservices practically eliminate downtime because, unlike monolithic architecture, services are separate and developers can update one component at a time.
              ### 3. Software can scale to keep up with business demand
              Development teams can work on single components without impacting the entire environment.
              ### 4. Lower costs and better revenue
              Microservices need less overall infrastructure, minimize downtime and improve stability for enhanced customer experience and reduce app prototype to deployment timeframes.
    - name: topics-copy-block
      data:
        header: Importance of a microservices architecture
        column_size: 10
        blocks:
            - text: |
                Today, microservices architecture is prevalent throughout the business world — many companies are either already utilizing it or they are moving towards it. Microservices save money, reduce engineering headaches, and minimize downtime.

                For engineers, microservices architecture allows for the building of scalable and resilient services that are easy to deploy.

                With continuous delivery, businesses can test functionalities by making different versions of services available to different subsets of their users. Even enterprises with existing monolithic software are exploring ways to modernize their stack with microservices.

                ### The benefits

                Clear boundaries between each microservice make the overall software application scalable, resilient, and autonomous.

                * **Scalable**: In a monolith, all application elements must scale together — meaning some parts could have to scale unnecessarily. With independent components, each microservice can scale individually according to its load.
                * **Resilient**: Failures can be contained using microservices. When one microservice fails, the rest of the application can remain online. It’s a good idea to display the status of each microservice on a dashboard of some kind so that maintenance personnel can identify issues and address problems quickly. Fixing issues can be as simple as moving a slider to bring an additional server online with on-demand virtualization.
                * **Autonomous**: Being autonomous means teams own the entire development cycle, from building to testing and all the way to deployment. They can select the best tools and language that suit the specific needs of the microservice. Additionally, they can release new services progressively so that customers may not even notice a deployment, meaning companies can make multiple deployments a day with confidence.

                ### The challenges

                Microservices bring the benefits of resiliency and scalability. However, making small independent services work together can be challenging.

                Let’s take a quick look at what you should keep in mind.

                * **Data consistency**: Microservices work together within an extensive system while maintaining their own data. That means there will be common data across multiple services. Changes must be replicated across all consumer services whenever there are changes to such common data.
                * **Risk of communication failure**: Unlike in-process function calls, calls to other services can fail over the network. That’s a necessary trade-off with the resiliency and scalability gained from the microservices architecture. To increase resiliency, you can use bulkhead and circuit breaker patterns.
    - name: topics-copy-block
      data:
        header: The future of microservices architecture
        column_size: 10
        blocks:
            - text: |
                As more businesses and individuals rely on SaaS applications, the need for availability and scalability rises. Supporting services like scalable databases, continuous delivery, observability tools, and data and event streaming are also growing rapidly.

                Engineers are continually gaining skills to help them overcome these challenges. Meanwhile, microservices architecture and network infrastructure are still maturing. It’s easy to foresee a future where microservice applications become easier to maintain and develop, and provide even more value to businesses.

    - name: topics-copy-block
      data:
        header: Using GitLab with microservices
        column_size: 10
        blocks:
            - text: |
                With GitLab, you can commit your code and have the tools you need in a single application. No more stitching together 10 tools for every project.

                Using a [DevOps platform](/topics/devops-platform/){data-ga-name="Devops platform"}{data-ga-location="body"} to manage your microservices helps you avoid information silos. Increasing visibility among teams and making handoffs easier leads to a faster DevOps lifecycle while also ensuring that your projects deploy and remain stable.

                A few ways GitLab simplifies microservice orchestration include:

                1. [Built-in CI/CD](/features/continuous-integration/){data-ga-name="Built in CI/CD"}{data-ga-location="body"}: As Fowler points out, infrastructure automation using continuous delivery and deployment is necessary for microservices. GitLab's built-in CI/CD is ideal for businesses looking to leverage microservices.
                1. [Built-in container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html){data-ga-name="Built in container registry"}{data-ga-location="body"} and a robust [Kubernetes integration](/solutions/kubernetes/){data-ga-name="Kubernetes integration"}{data-ga-location="body"}: While microservices architecture can be used with legacy VM technology, containers and Kubernetes make building microservices significantly easier. GitLab is designed to work well with Kubernetes.
                1. [Built-in Monitoring](/stages-devops-lifecycle/monitor/){data-ga-name="Built in monitoring"}{data-ga-location="body"}: Monitoring is critical to a successful operation. GitLab's monitoring capabilities leveraging Prometheus make GitLab ideal for microservices.
                1. [Multi-project pipelines](/blog/2018/10/31/use-multiproject-pipelines-with-gitlab-cicd/){data-ga-name="Multi project pipelines"}{data-ga-location="body"} support running pipelines with cross-project dependencies.
                1. Monorepo support with the ability to [run a pipeline only when code a specific directory changes](https://docs.gitlab.com/ee/ci/yaml/#only-and-except-simplified){data-ga-name="Only and except"}{data-ga-location="body"}.
                1. [Group-level Kubernetes clusters](https://docs.gitlab.com/ee/user/group/clusters/){data-ga-name="Group-level kubernetes"}{data-ga-location="body"} allow multiple projects to integrate with a single Kubernetes cluster.
    - name: topics-copy-block
      data:
        column_size: 10
        header: GitLab key benefits
        blocks:
            - text: |
                GitLab is a single DevOps platform attracting top developers looking to collaborate and improve. Below are some key benefits of becoming a GitLab advocate:

                * **No switching between apps** - Developers don't need to constantly context-switch, making it easier to focus and stay on task. Additionally, you can easily link issues to work, increase productivity and eliminate software engineers' main bugbears.
                * **Reduce your workload** - GitLab makes it easier to automate tasks so that you can focus on more demanding, results-based activities. Repetitive manual tasks take the joy out of a developer's workday. Automate tasks to improve work satisfaction and productivity.
                * **Collaboration and transparency** - Collaboration and transparency are key benefits of the GitLab platform. Abject transparency between developer and customer facilitates collaboration for quick, easy fixes and less communication.
                * **Be part of the community** - All developers are free to contribute to GitLab's open-source core to improve the app. It's a warm, friendly and highly responsive community that welcomes new community members and helps them learn the ropes.
                * **Learn cutting-edge software development practices** - Coding and app development is one of the fastest-growing fields in an increasingly fast-paced world. GitLab updates its community on all the latest developments and helps them learn best practices.
    - name: topics-cta
      data:
        title: Start your cloud native transformation
        subtitle: Build, test, deploy, and monitor your code from a single application.
        text: |
          Hear how Ask Media Group migrated from on-prem servers to the AWS cloud with GitLab tools and integrations. Join us and learn from their experience.
        column_size: 8
        cta_one:
          text: Save your spot!
          link: /webcast/cloud-native-transformation/
          data_ga_name: Save your spot
          data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: video
              variant: marketing
              alt: Video Icon
            event_type: "Video"
            header: Mastering Chaos - A Netflix Guide to Microservices
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_13.jpeg"
            href: "https://www.youtube.com/watch?v=CZ3wIuvmHeM"
            data_ga_name: Mastering Chaos - A Netflix Guide to Microservices
            data_ga_location: resource cards
          - icon:
              name: video
              variant: marketing
              alt: Marketing Icon
            event_type: "Video"
            header: Guilt's move from monolith to microservices
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_16.jpg"
            href: "https://www.youtube.com/watch?v=C4c0pkY4NgQ"
            data_ga_name: Guilt's move from monolith to microservices
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Article"
            header: How to break a Monolith into Microservices
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_8.jpeg"
            href: "https://martinfowler.com/articles/break-monolith-into-microservices.html"
            data_ga_name: How to break a Monolith into Microservices
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Article"
            header: Evolution of business logic from monoliths through microservices, to functions
            link_text: "Learn more"
            image: "/nuxt-images/resources/resources_15.jpeg"
            href: "https://read.acloud.guru/evolution-of-business-logic-from-monoliths-through-microservices-to-functions-ff464b95a44d"
            data_ga_name: Evolution of business logic from monoliths through microservices, to functions
            data_ga_location: resource cards
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: "Trends in Version Control Land: Microservices"
            text: |
              The benefits and drawbacks of microservices and how to decide if it is right for your team.
            link_text: "Learn more"
            href: /blog/2016/08/16/trends-in-version-control-land-microservices/
            image: /nuxt-images/blogimages/trends-in-version-control-land-microservices-cover.jpg
            data_ga_name: "Trends in Version Control Land: Microservices"
            data_ga_location: resource cards
          - header: "It's raining repos: The microservices repo explosion, and what we're doing about it"
            icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            text: |
              Microservices have spawned an explosion of dependent projects with multiple repos, creating the need for an integrated solution - we're working on it right now.
            link_text: Learn more
            href: /blog/2018/11/26/microservices-integrated-solution/
            image: /nuxt-images/blogimages/microservices-explosion.jpg
            data_ga_name: "It's raining repos: The microservices repo explosion, and what we're doing about it"
            data_ga_location: resource cards
          - header: Implementing microservices architectures and deployment strategies
            icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            text: |
              Want to dump the monolith and get into microservices? Consider these three methods.
            link_text: Learn more
            href: /blog/2019/06/17/strategies-microservices-architecture/
            image: "/nuxt-images/resources/resources_17.jpg"
            data_ga_name: "Implementing microservices architectures and deployment strategies"
            data_ga_location: resource cards
          - header: How to manage Agile teams with microservices
            icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            text: |
              GitLab Groups and Projects can help teams divide work by product or system.
            link_text: Learn more
            href: /blog/2019/08/23/manage-agile-teams-with-microservices/
            image: /nuxt-images/blogimages/agilemultipleteams.jpg
            data_ga_name: "How to manage Agile teams with microservices"
            data_ga_location: resource cards
          - header: 'From monolith to microservices: How to leverage AWS with GitLab'
            icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            text: |
              GitLab recently spent time with Ask Media Group and AWS to discuss how modernizing from self-managed to a cloud native system empowers developers.
            link_text: Learn more
            href: /blog/2020/03/24/from-monolith-to-microservices-how-to-leverage-aws-with-gitlab/
            image: /nuxt-images/blogimages/askmediablog-.jpg
            data_ga_name: "From monolith to microservices: How to leverage AWS with GitLab"
            data_ga_location: resource cards
          - header: What devs need to know about tomorrow's tech today
            icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            text: |
              From 5G to edge computing, microservices and more, cutting-edge technologies will be mainstream soon. We asked more than a dozen DevOps practitioners and analysts which technologies developers need to start to understand today.
            link_text: Learn more
            href: /blog/2020/10/21/how-tomorrows-tech-affects-sw-dev/
            image: /nuxt-images/blogimages/future-of-software-what-developers-need-to-know.png
            data_ga_name: "Trends in Version Control Land: Microservices"
            data_ga_location: resource cards
