---
  title: GitLab Terms of Use
  description: Here you can find information on the terms related to GitLab's Website and your use of GitLab Software
  components:
    - name: 'call-to-action'
      data:
        title: GitLab Terms of Use
        subtitle: Thank you for choosing GitLab! For the current terms please see the Current Terms of Use table below.
        horizontal_rule: true
        centered_by_default: true
    - name: 'copy'
      data:
        block:
          - text: |
              The **CURRENT TERMS OF USE** (unless otherwise stated in the Table below) shall apply to net-new and renewal purchases made on, or after, September 7, 2022. If you received an Order Form / Quote, or purchased, prior to September 7, 2022, please see the Agreement History below for the terms applicable to your purchase and/or use of GitLab software. Any GitLab Subscription purchased prior to September 7, 2022 including upgrades and additional Users purchased for that Subscription, will be governed by the Agreement in effect as of the Subscription purchase date.
            hide_horizontal_rule: true
    - name: 'slp-table'
      data:
        title: CURRENT TERMS OF USE
        fullwidth: true
        headers:
          - Type of Use / Activity
          - Applicable Agreement
        rows:
          - columns:
            - Use of any [GitLab Software Offering](/pricing/){data-ga-name="GitLab Pricing | GitLab" data-ga-location="body"}, including Free tier
            - '[Subscription Agreement](/handbook/legal/subscription-agreement/){data-ga-name="GitLab Subscription Agreement | GitLab" data-ga-location="body"}'
          - columns:
            - |
                Use of any GitLab Professional Services

                _(Effective as of November 1, 2021)_
            - '[Professional Services Agreement](/handbook/legal/professional-services-agreement/){data-ga-name="GitLab Professional Services Agreement | GitLab" data-ga-location="body"}'
          - columns:
            - Privacy Statement
            - '[GitLab Privacy Statement](/privacy/){data-ga-name="GitLab Privacy Policy | GitLab" data-ga-location="body"}'
          - columns:
            - GitLab Processing Personal Data from an Enterprise
            - '[GitLab Data Processing Agreement and Standard Contractual Clauses](/handbook/legal/data-processing-agreement/){data-ga-name="GitLab Data Processing Agreement | GitLab" data-ga-location="body"}'
          - columns:
            - Request Removal of Content / Data
            - '[DMCA Notice and Take Down](/handbook/dmca/){data-ga-name="DMCA Policy | GitLab" data-ga-location="body"}'
          - columns:
            - Use of GitLab's Website(s)
            - '[Website Terms of Use](/handbook/legal/policies/website-terms-of-use/){data-ga-name="GitLab Website Terms of Use | GitLab" data-ga-location="body"}'
          - columns:
            - Cookies & Visiting the Website
            - '[GitLab Cookie Policy](/privacy/cookies/){data-ga-name="GitLab Cookies Policy | GitLab" data-ga-location="body"}'
          - columns:
            - Using GitLab’s publicly-available APIs
            - '[GitLab API Terms of Use](https://about.gitlab.com/handbook/legal/api-terms/){data-ga-name="GitLab API Terms of Use" data-ga-location="body"}'
          - columns:
            - Partner Program
            - Enroll and Terms please visit the [GitLab Partner Program](https://partners.gitlab.com/English/){data-ga-name="Gitlab Partner Program | Home" data-ga-location="body"}
          - columns:
            - GitLab for Education Program*
            - '[GitLab for Education Program Agreement](/handbook/legal/education-agreement/){data-ga-name="GitLab for Education Program Agreement | GitLab" data-ga-location="body"}'
          - columns:
            - GitLab for Open Source Program**
            - '[GitLab for Open Source Program Agreement](/handbook/legal/opensource-agreement/){data-ga-name="GitLab for Open Source Program Agreement | GitLab" data-ga-location="body"}'
    - name: 'copy'
      data:
        block:
          - text: |
              _*Only applicable for Educational Institutions using GitLab Software for Instructional Use, or, Non-Commercial Academic Research. Please visit [Program Guidelines](/solutions/education/join/){data-ga-name="Join the GitLab for Education Program | GitLab" data-ga-location="body"} for more information."_

              _**Only applicable for Open Source Organizations using GitLab Software for Open Source Software. Please visit [Program Guidelines](/solutions/open-source/join/){data-ga-name="Join the GitLab for Open Source Program | GitLab" data-ga-location="body"} for more information._
            hide_horizontal_rule: true
    - name: 'slp-table'
      data:
        title: AGREEMENT HISTORY
        text:  |
          GitLab, including GitLab Legal, is committed to transparency as part of its’ values, as such we provide previous versions of our Agreements.
        fullwidth: true
        headers:
          - Legacy Agreement and Applicable Dates
          - Agreement Location
        rows:
          - columns:
            - Subscription Agreement (August 1, 2021 - September 06, 2022)
            - '[Legacy Subscription Agreement V3](/handbook/legal/legacy-subscription-agreement-v3/){data-ga-name="GitLab Subscription Agreement | GitLab" data-ga-location="body"}'
          - columns:
            - Subscription Agreement (February 1, 2021 - July 31, 2021)
            - '[Legacy Subscription Agreement V2](/handbook/legal/legacy-subscription-agreement-v2/){data-ga-name="GitLab Subscription Agreement | GitLab" data-ga-location="body"}'
          - columns:
            - Subscription Agreement (January 1, 2015 - January 31, 2021)
            - '[Legacy Subscription Agreement V1](/terms/signature.html){data-ga-name="Legacy Subscription Agreement | GitLab" data-ga-location="body"}'
          - columns:
            - Professional Services Agreement (January 1, 2015 - October 31, 2021)
            - '[Legacy Professional Services Agreement V1](/handbook/legal/legacypsav1/){data-ga-name="Legacy v1 - GitLab Professional Services Agreement | GitLab" data-ga-location="body"}'



